var TIAA_ud = TIAA_ud || {};

TIAA_ud = {
	udThemeUrl: '/themes/ud_atom/release_2013-12/',
	globalAssetUrl: '/assets/',
	globalThemeUrl: '/assets/release_2013-12/',
	localUrl: 'assets/',
	cssName: 'ud_ud_proto_myadvisor.css',		// Proj Specific CSS	
	jsName: 'ud_proto_myadvisor.js',	// Proj Specific JS
	projLoc: '/ud/myadvisor/'					// Location of currect project from prototype root
};


// Loading CSS
document.write('<link type="text/css" rel="stylesheet" href="' + TIAA_ud.udThemeUrl + 'css/global.css">');
document.write('<link type="text/css" rel="stylesheet" href="' + TIAA_ud.udThemeUrl + 'css/print_ud_atom.css" media="print">');
document.write('<link type="text/css" rel="stylesheet" href="' + TIAA_ud.udThemeUrl + 'css/tablet_ud_atom.css" media="only screen and (max-device-width: 1024px)">');

   
// Load the main Script            
document.write('<script src="'+TIAA_ud.udThemeUrl+'js/ud_main.js'+'"></script>');