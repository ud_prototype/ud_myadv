$(document).ready(function(){
	/* Start Bio Lookup Search functionality*/

	var winQ = window.location.href.split('?');
	console.log(winQ[1]);
	if(winQ[1] != undefined){
		$(".successMsg").removeClass('closed');
	}else{
		$(".successMsg").addClass('closed');
	}
	$('#fileUpload').on('click', function() {
		$('#fileUploadHidden').trigger('click');
	});
	$('#fileUploadHidden').change(function() {
		var $thisVal = $(this).val(),
			$chkJpg = $thisVal.split(".");
		if($chkJpg[1] != "jpg"){
			$('#selectedFile').html("<span class='descl'>Please choose .jpeg file format</span>");
			$('#uploadSave').on('click', function(){
				var $imgPath = $('#selectedFile').html();
				$('#changeImage').attr('src','assets/images/no_photo.png');	
			});
		}else{
			$('#selectedFile').html($thisVal);
			$('#uploadSave').on('click', function(){
				var $imgPath = $('#selectedFile').html();
				$('#changeImage').attr('src','assets/images/julie_photo.png');	
			});
		}
	});
	$( "#editPhotoForm").find('.btnBar button').on('click', function(){
		$( "#editPhotoForm" ).dialog( "close" );
	})
	var $fbS = $('#blFilterBy'),
		$si = $('#blSearchInp'),
		$blTbl = $('#blDispDataTbl'),
		$srcBtn = $('#isBioLookupForm');
	$fbS.on('change',function(e) {
		var $fbV = $(this).val();
		var datainst =  $('option:selected', $(e.currentTarget)).attr('data-instruction'); 		   
				datainst = (typeof datainst == 'undefined') ? '' : datainst;
				$si.val(datainst); 
	}); 
	$si.on('focus',function(){
		var $cV = $(this).val();
		if($cV == '- Enter Name -' || $cV == '- Enter Title -' || $cV == '- Enter Location -'){ 
			this.value =''; 
		}
	});
	$srcBtn.on('submit',function(e){
			e.preventDefault();
			var $siV = $si.val(),
				$sdV = $('#blFilterBy option:selected').attr('data-instruction');
			if($siV == '' || $siV == '- Enter Name -' || $siV == '- Enter Title -' || $siV == '- Enter Location -' || $sdV == ''){
				$blTbl.addClass('closed');
				$si.focus();
			}else{
				$blTbl.removeClass('closed');
			}
		}); 
	/* End Bio Lookup Search functionality*/
	
	/*Start Add and Remove list functionality*/	
  $('.items li').on('click', function() {
        if($(this).attr('class')=='selected shd1') {
			$(this).removeClass('selected shd1');
		}
		else {
			$(this).addClass('selected shd1');
		}
  });
	
	$('.insertItems').on('click', function() {
		var element_id = $(this).attr('id'),
			primarydiv = element_id.replace('add_', '')+1,
			secondarydiv = element_id.replace('add_', '')+2;
	    $("#"+primarydiv+" ul li input:checkbox[name='contactList_checkbox']:checked").each(function() {
			$(this).closest('ul li').removeClass('selected shd1').appendTo('#'+secondarydiv+' ul');
			$(this).closest('ul li').find('input[type=checkbox]').removeAttr('checked');
			var user = secondarydiv.split('_');
			if($('#'+user[0]+'_'+user[1]+'_NewReport').val() !='') $('#'+user[0]+'_'+user[1]+'_disEnbBioSave').removeClass('btnOff');
		});
	});
	
	
	$('.deleteItems').on('click', function() {
		var element_id = $(this).attr('id'),
			primarydiv = element_id.replace('remove_', '')+2,
			secondarydiv = element_id.replace('remove_', '')+1,
			user = primarydiv.split('_');
		$("#"+primarydiv+" ul li input:checkbox[name='contactList_checkbox']:checked").each(function() {
			$(this).closest('ul li').removeClass('selected shd1').appendTo('#'+secondarydiv+' ul');
			$(this).closest('ul li').find('input[type=checkbox]').removeAttr('checked');
		});
		var listavailable = $('#'+user[0]+'_'+user[1]+'_teamBio2 ul li').length;
		if(listavailable ==0) $('#'+user[0]+'_'+user[1]+'_disEnbBioSave').addClass('btnOff');

	});
	
	/*End Add and Remove list functionality*/	
	
	/*Start Create ne Bio Team List*/
	$('a[href=#teamBio-popup]').on('click', function(){
		$('#nameNewReportBio').val('');		
		$('.body').find('.alertModule').removeClass('visible');
	});
	$('a[href=#teamcontactList-popup]').on('click', function(){
		$('#nameNewReportContact').val('');		
	});
	
	$('#nameNewReportBio, #nameNewReportContact').on('keypress keyup',function(){
		var $ulChkB = $('#teamBio2 ul');
		var $ulChkC = $('#teamcontactList2 ul');
		var $inLen = $(this).val();
		if(($ulChkB.html() == '' && $inLen == '') || ($ulChkC.html() == '' && $inLen == '')){
			
		}else{
				
		}
	});
	$('.disEnbBioSave').on('click', function(){
		var $rn = $(this).parents('.body').find('.repname').val();
		if($rn != ""){
			$('a[href=#closePopup]').trigger('click');
			$(this).parents('.body').find('.alertModule').removeClass('visible');
			var $fid = $(this).attr('id'),
			$user = $fid.split('_'),
			$fVb = $('#'+$user[0]+'_'+$user[1]+'_NewReport').val(),
			$aRow = '<tr><td><a href="#">'+$fVb+'</a><div class="flr">';
			if($user[0] != 'RM' && $user[0] != 'NRM') $aRow = $aRow +'<span class=" mrlg icclearLink icon"></span>';
			 $aRow = $aRow +'<span class=" mrlg iceditLink icon"></span><span class=" mrlg icprintLink icon"></span></div></td>';
					if($user[0] != 'NRM' && $user[0] !='MA') $aRow = $aRow +'<td><a href="#">Individual Listing</a><div class="flr">';
					if($user[0] == 'ADM') $aRow = $aRow +'<span class=" mrlg icclearLink icon"></span><span class=" mrlg iceditLink icon"></span>';
					
					if($user[0] !='NRM' && $user[0] !='REP' && $user[0] !='MA') $aRow = $aRow +'<span class=" mrlg icprintLink icon"></div>';
					$aRow = $aRow +'</td><td></td></tr>';
					
		$('#tbl_reports tr:last').after($aRow);
		$('#'+$user[0]+'_'+$user[1]+'_teamBio2 ul').html('');
		$('.repname').val('');
	}else{
		$(this).parents('.body').find('.alertModule').addClass('visible');
		$('.repname').val('');
	}
	});
	
	
	$('#disEnbContactSave').on('click', function(){
		var $fVc = $('#nameNewReportContact').val(),
			$aRow = '<tr><td><a href="#">'+$fVc+'</a><div class="flr"><span class="mts mrlg icclearLink icon"></span><span class="mts mrlg iceditLink icon"></span>'+
					'<span class="mts mrlg icprintLink icon"></span></div></td><td><a href="#">Individual Listing</a></td><td></td></tr>';
		$('#tbl_reports tr:last').after($aRow);
		$('#addLastReport').html($fVc);
		$('a[href=#closePopup]').trigger('click');
		$('#teamcontactList2 ul').html('');
		$('#teamBio2 ul').html('');
	});
	/*End Create ne Bio Team List*/
	
	$('#ebtn').on('click', function() {
		$(".successMsg").addClass('closed');
		$('.dclass, .eeclass, .epclass').addClass('closed');
		$('.eclass, .esclass').removeClass('closed');
		$('#photoDisp').addClass('closed');
		$('#photoEdit').removeClass('closed');
		$('.addMoreCol').removeClass('closed');
		$(".alertMsg").addClass('closed');
		
	});
	
	$('#cncl').on('click', function() {
		$('.dclass, .eeclass, .dsclass').removeClass('closed');
		$('.eclass, .esclass, .successMsg, .sclass').addClass('closed');
		$(".alertMsg").removeClass('closed');
		$("#offi, #MailingAdd-field, #Suite-field").val('');
	});
	$('#offi').on('change', function(e) {
		var chV = $(this).val();
		var tdII =  $('option:selected', $(e.currentTarget)).attr('data-instruction'); 	
		var tdI = tdII.split('-');
		
		if(chV=="Marlon, NJ"){
		$('.sclass').removeClass('closed');
		}else{
		$('.sclass').addClass('closed');
		
		}
		
		$("#city").val(chV);
		
		
		$("#MailingAdd-field").val('155 Village Blvd');
		$("#Suite-field").val('Suite '+tdI[1]);
		$("#state-field").val(tdI[0]);
		$("#Zip-field").val(tdI[2]);
		$("#Floor-field").val(tdI[3]);
		$("#MailingAdd-field").val(tdI[4]);
		
		$('.dsclass').addClass('closed');
	});
	
	$('#additems').on('click', function() {
	    if(!$('#additems').hasClass('btnOff')) {
			$('#reportlist1 li.selected').appendTo('#reportlist2');
			$('#reportlist1 li').length ==0 ? $('#additems').addClass('btnOff'):'';
			$('#reportlist2 li').length >0 ? $('#removeitems').removeClass('btnOff'):'';
		}
	});
	
	$('#removeitems').on('click', function() {
		if(!$('#removeitems').hasClass('btnOff')) {
			$('#reportlist2 li.selected').appendTo('#reportlist1');
			$('#reportlist2 li').length ==0 ? $('#removeitems').addClass('btnOff'):'';
			$('#reportlist1 li').length >0 ? $('#additems').removeClass('btnOff'):'';
		}	
	});
	
	$('#srcprofile').on('click',function() {
		$('#profilestatus').removeClass('closed');
	 }); 
	 var cnt = 4;
	 $('#terri').on('change',function(){
	 cnt++;
	 var $thisV = $(this).val();
		var $insInp = '<div class="lblFieldPair nth mtm"><div class="input">'+
					  '<input id="addlterri'+cnt+'-field" type="text" aria-required="true" name="addlterri'+cnt+'-field" value="'+$thisV+'">'+
					  '</div></div>';
		$(this).closest('div.lblFieldPair').before($insInp);
	 });

	 $('.repname').on('keypress keyup',function(){
		var savebtnid = $(this).attr('id');
		var user = savebtnid.split('_');
	    if($(this).val() !=='') {	    
		var listavailable = $('#'+user[0]+'_'+user[1]+'_teamBio2 ul li').length;
		if(listavailable >0) $('#'+user[0]+'_'+user[1]+'_disEnbBioSave');	
		}
		else $('#'+user[0]+'_'+user[1]+'_disEnbBioSave');		
	 });
	 
	 var counter = 0;
	 $("#addMore").click(function(){
	  if (counter > 2) {
     alert("Only 3 rows allowed");
     return false;
     }
     $("#PD1 > tbody").append('<tr id="row'+counter+'"><td>&nbsp;</td><td></td></tr>');
    counter++;
    });

});